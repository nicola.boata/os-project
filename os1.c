#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

void parseDir(char *path) {
    DIR *dir;
    struct dirent *entry;

    if (!(dir = opendir(path))) {
        printf("=== Error in opening directory ===\n");
        exit(0);
    } else {
        while ((entry = readdir(dir)) != NULL) {
            printf("%s/%s\n", path, entry->d_name);
        }
        closedir(dir);
    }
}

int main(int argc, char *argv[]) {
    if (argc <= 1) {
        printf("=== Too few arguments ===\n");
        exit(1);
    }
    if (argc == 2) {
        parseDir(argv[1]);
    }
    return 0;
}
