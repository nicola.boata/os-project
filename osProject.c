#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <dirent.h>
#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>
#include <sys/wait.h>

#define MAX_DIRS 10
#define MAX_FILES 100
#define MAX_NAME_LENGTH 50 //of directory
#define MAX_PATH_LENGTH 100 //of directory
#define MAX_LINE_LENGTH 1000

typedef struct {
    char name[MAX_NAME_LENGTH];
} file;

typedef struct {
    char name[MAX_NAME_LENGTH];
    char path[MAX_PATH_LENGTH];
    file files[MAX_FILES];
    int file_count;
} directory;

directory directories[MAX_DIRS];
int dir_count = 0;

void replace_char(char *str, char old_char, char new_char);
char* GetNameOfFile(char* path);
void make3rdPartyFile(const char* filename);
bool compare(char* filename);
void saveDir(const char* path, const char* izolated_dir);
void createFile(const char* dirname);
void addDirs(char** argv, int argc);
void getFileInformation(const char* path, struct stat* fileStat);
bool checkIfMalicious(const char* path);

char* nameOfFile(char* path) {
    char *nameOfFile;
    nameOfFile = strrchr(path, '/');
    if (nameOfFile != NULL) {
        nameOfFile++; 
    } else {
        nameOfFile = path;
    }
    return nameOfFile;
}

void replace_char(char *str, char old_char, char new_char) {
    while (*str) {
        if (*str == old_char) {
            *str = new_char;
        }
        str++;
    }
}


//uses the flags O_WRONLY to open the file for writing only, 
//O_CREAT to create the file if it does not exist,
//and O_TRUNC to truncate the file to zero length if it exists
//the mode 0644 specifies the permissions of the file.
void make3rdPartyFile(const char* filename) {
    int a = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0644);//0644-owner has read and write permissions, while the group and others have only read permissions
    struct stat fileStat;
    getFileInformation(filename, &fileStat); 
    write_file_info(a, &fileStat, filename);
    close(a);
}

bool compare(char* filename) {
    pid_t pid = fork();
    if (pid == -1) {
        perror("fork");
        return false;
    } else if (pid == 0) { //child process
        execlp("bash", "bash", "./comparation.sh", "third_party.txt", filename, NULL);
        perror("execlp");
        exit(1);
    } else { //parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status)) {
            int exit_status = WEXITSTATUS(status);
            return exit_status != 0; //diff found if exit_status != 0
        } else {
            printf("Child process did not terminate normally\n");
            return false;
        }
    }
}

void saveDir(const char* path, const char* izolated_dir) {
    DIR* dir;
    struct dirent *entry;
    struct stat info;

    dir = opendir(path);
    if (dir == NULL) {
        perror("opendir");
        exit(1);
    }

    strcpy(directories[dir_count].name, GetNameOfFile(path));
    strcpy(directories[dir_count].path, path);
    directories[dir_count].file_count = 0;

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, "..") == 0 || strcmp(entry->d_name, ".") == 0) {
            continue; 
        }

        char full_path[MAX_PATH_LENGTH];
        snprintf(full_path, MAX_PATH_LENGTH, "%s/%s", path, entry->d_name);
        
        if (stat(full_path, &info) == -1) {
            perror("stat");
            continue;
        }

        if (S_ISDIR(info.st_mode)) {
            saveDir(full_path, izolated_dir);
        } else {
            // Handling files
            if (directories[dir_count].file_count < MAX_FILES) {
                strcpy(directories[dir_count].files[directories[dir_count].file_count].name, entry->d_name);
                directories[dir_count].file_count++;
            } else {
                fprintf(stderr, "==Maximum file count reached for directory: %s==\n", path);
            }
        }
    }

    closedir(dir);
    dir_count++;
}

void createFile(const char* dirname) {
    for (int i = 0; i < dir_count; i++) {
        char filename[MAX_PATH_LENGTH];
        snprintf(filename, MAX_PATH_LENGTH, "%s/%s_characteristics.txt", dirname, directories[i].name);
        
        // Write file information to the file

        for (int j = 0; j < directories[i].file_count; j++) {
            snprintf(filename, MAX_PATH_LENGTH, "%s/%s-%s_characteristics.txt", dirname, directories[i].name, directories[i].files[j].name);
            
            // Write file information to the file
        }
    }
}

void addDirs(char** argv, int argc) {
    if (argc < 4 || strcmp(argv[1], "-o") != 0 || strcmp(argv[3], "-m") != 0) {
        fprintf(stderr, "==Usage: %s -o <output_directory> -m <izolated_dir> <directory1> <directory2> ... <directoryN>==\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    struct stat path_stat;
    if (stat(argv[2], &path_stat) != 0 || !S_ISDIR(path_stat.st_mode)) {
        fprintf(stderr, "==Error: Output directory %s is not valid or could not be accessed.==\n", argv[2]);
        exit(EXIT_FAILURE);
    }

    if (stat(argv[4], &path_stat) != 0 || !S_ISDIR(path_stat.st_mode)) {
        fprintf(stderr, "==Error: Vulnerable directory %s is not valid or could not be accessed.==\n", argv[4]);
        exit(EXIT_FAILURE);
    }

    for (int i = 5; i < argc; i++) {
        if (stat(argv[i], &path_stat) != 0 || !S_ISDIR(path_stat.st_mode)) {
            fprintf(stderr, "==Error: Directory %s is not valid or could not be accessed.==\n", argv[i]);
        } else {
            saveDir(argv[i], argv[4]);
            createFile(argv[2]);
        }
    }
}

void getFileInformation(const char* path, struct stat* fileStat) {
    if (stat(path, fileStat) == -1) {
        perror("==Error getting file information==");
        exit(EXIT_FAILURE);
    }
}

bool checkIfMalicious(const char* path) {
    pid_t pid = fork();
    if (pid == -1) {
        perror("fork");
        return false;
    } else if (pid == 0) { // Child process
        execlp("bash", "bash", "./search_mal.sh", path, NULL);
        perror("execlp");
        exit(EXIT_FAILURE);
    } else { // Parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status)) {
            int exit_status = WEXITSTATUS(status);
            return exit_status != 0; // malitious data found if exit_status != 0
        } else {
            printf("==Child process did not terminate normally==\n");
            return false;
        }
    }
}

int main(int argc, char *argv[]) {
    addDirs(argv, argc);
    return 0;
}
