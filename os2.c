#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

// Function to recursively traverse directories and create snapshot
void createSnapshot(char *dirName, FILE *snapshotFile, int depth){
    DIR *dir;
    struct dirent *entry;
    struct stat fileStat;
    char path[1024];

    //open directory
    if(!(dir = opendir(dirName))){
        printf("== Can't open directory ==\n");
        exit(0);
    }

    //read directory contents
    while((entry = readdir(dir)) != NULL){
        // skip "." and ".."
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") ==0 )
            continue;

        //path of entry
        snprintf(path, sizeof(path), "%s/%s", dirName, entry->d_name);

        //file status
        if(lstat(path, &fileStat) < 0){
            fprintf(stderr, "== Can't stat %s ==\n", path);
            continue;
        }

        //indent according to depth
        for(int i = 0; i < depth; i++){
            fprintf(snapshotFile, "| ");
        }

        fprintf(snapshotFile, "|_%s\n", entry->d_name);

        //if entry is a directory we recursively call createSnapshot()
        if (S_ISDIR(fileStat.st_mode))
            createSnapshot(path, snapshotFile, depth + 1);
    }

    closedir(dir);
}

int main(int argc, char *argv[]){
    
    if(argc != 2){
        printf("=== Too few arguments ===\n");
        exit(1);
    }

    //snapshot file
    FILE *snapshotFile = fopen("Snapshot.txt", "w");
    if(!snapshotFile){
        printf("== Can't create Snapshot.txt ==\n");
        exit(2);
    }

    fprintf(snapshotFile, "%s\n", argv[1]);

    //create snapshot
    createSnapshot(argv[1], snapshotFile, 1);

    fclose(snapshotFile);

    printf("== Snapshot was created successfully ==\n");

    return 0;
}
